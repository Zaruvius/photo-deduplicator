## Photo Deduplicator

This application uses a SHA256 hash to check whether two files are the same. A dictionary keeps track of compared files and moves files into seperate folders depending on whether it's a duplicate or not. 
