import hashlib
import shutil
import os


def get_byte_sha256_hash(bytes_to_hash):
    hasher = hashlib.sha256()
    hasher.update(bytes_to_hash)

    return hasher.hexdigest()


def get_file_contents(full_file_name):
    with open(full_file_name, "rb") as file:
        return file.read()


def get_file_hash(full_file_name):
    return get_byte_sha256_hash(get_file_contents(full_file_name))


def move_file(src_full_name, dest_full_name):
    shutil.move(src_full_name, dest_full_name)


def get_files_in_folder(folder_name):
    return os.listdir(folder_name)


def combine(directory, file_name):
    return os.path.join(directory, file_name)


def process_files(original_folder, keep_folder, duplicate_folder):
    file_hashes = {}
    dups_processed = 0
    files_to_dedup = get_files_in_folder(original_folder)

    for file in files_to_dedup:
        print("Processing {0}...".format(file))

        file_hash = get_file_hash(original_folder + file)

        if file_hash in file_hashes:
            dups_processed = dups_processed + 1
            move_file(combine(original_folder, file), combine(duplicate_folder, file))
        else:
            file_hashes[file_hash] = file
            move_file(combine(original_folder, file), combine(keep_folder, file))

    print("completed processing {0} files, and processed {1} duplicates".format(len(files_to_dedup), dups_processed))


orig_folder = "c:\\develop\\Python\\photo-deduplicator\\original\\"
keep_folder = "c:\\develop\\Python\\photo-deduplicator\\keep\\"
dup_folder = "c:\\develop\\Python\\photo-deduplicator\\duplicates\\"

process_files(orig_folder, keep_folder, dup_folder)
